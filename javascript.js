
// Manejar el evento de registros

function registrar(event) {
			event.preventDefault();

			const usernameInput = document.querySelector('#username'); 
			const emailInput = document.querySelector('#email'); 
			const passwordInput = document.querySelector('#password'); 
			const confirmPasswordInput = document.querySelector('#confirm-password'); 

			if (usernameInput.value === "" || emailInput.value === "" || passwordInput.value === "") {
			alert("Por favor, complete todos los campos");
			return false;
			}

			if (passwordInput.value !== confirmPasswordInput.value) {
			alert("Las contraseñas no coinciden"); 
			return false;
			}

			sessionStorage.setItem("username", usernameInput.value);
			sessionStorage.setItem("email", emailInput.value);
			sessionStorage.setItem("password", passwordInput.value);

			alert("¡Registrado correctamente!");
			window.location.href = "login.html";
}



// Manejar el evento de inicio de sesión

function iniciarSesion(event) {
	
		  event.preventDefault();
		  const usernameInput = document.querySelector('#login-username'); 
		  const passwordInput = document.querySelector('#login-password'); 

		  const storedEmail = sessionStorage.getItem("#email");
		  const storedPassword = sessionStorage.getItem("#password");

		  if (usernameInput.value !== storedEmail || passwordInput.value !== storedPassword) {
			alert("¡Logeado correctamente!");
		    window.location.href = "habitacion.html";
			return false;
		  }

		  // Iniciar sesión exitosamente
			alert("¡Logeado correctamente!");
		  window.location.href = "habitacion.html";
}

// Manejar el evento de reservas estudiantes

const enviarBtn = document.querySelector('.enviarbtn');
const metodoPago = document.querySelector('#metodo-pago');
const tarjeta = document.querySelector('#tarjeta');
const transferencia = document.querySelector('#transferencia');
const paypal = document.querySelector('#paypal');
const numeroTarjeta = document.querySelector('#numero-tarjeta');
const nombreTarjeta = document.querySelector('#nombre-tarjeta');
const fechaVencimiento = document.querySelector('#fecha-vencimiento');
const codigoSeguridad = document.querySelector('#codigo-seguridad');
const banco = document.querySelector('#banco');
const cuenta = document.querySelector('#cuenta');
const correoPaypal = document.querySelector('#correo-paypal');

// Ocultar campos adicionales al cargar la página
tarjeta.classList.add('hidden');
transferencia.classList.add('hidden');
paypal.classList.add('hidden');

// Mostrar campos adicionales según el método de pago seleccionado
metodoPago.addEventListener('change', function() {
  switch(metodoPago.value) {
    case 'tarjeta':
      tarjeta.classList.remove('hidden');
      transferencia.classList.add('hidden');
      paypal.classList.add('hidden');
      break;
    case 'transferencia':
      tarjeta.classList.add('hidden');
      transferencia.classList.remove('hidden');
      paypal.classList.add('hidden');
      break;
    case 'paypal':
      tarjeta.classList.add('hidden');
      transferencia.classList.add('hidden');
      paypal.classList.remove('hidden');
      break;
    default:
      tarjeta.classList.add('hidden');
      transferencia.classList.add('hidden');
      paypal.classList.add('hidden');
  }
});

// Manejar el evento de clic en el botón "Enviar" de verificar datos
enviarBtn.addEventListener('click', function() {
  // Validar que se hayan ingresado los datos requeridos
  if (metodoPago.value === "" ||
      (metodoPago.value === "tarjeta" && (numeroTarjeta.value === "" || nombreTarjeta.value === "" || fechaVencimiento.value === "" || codigoSeguridad.value === "")) ||
      (metodoPago.value === "transferencia" && (banco.value === "" || cuenta.value === "")) ||
      (metodoPago.value === "paypal" && correoPaypal.value === "")) {
    alert("Por favor complete todos los campos obligatorios");
    return;
  }


  // Actualizar el mensaje de éxito después de enviar el formulario
  alert("Has reservado correctamente tu habitación");
  window.location.href = "habitacion.html";
});


function mostrarFormularioAdmin() {
  // mostrar el formulario de inicio de sesión
  $('#formulario-login').show();
  
  // centrar el formulario de inicio de sesión en la pantalla
  var anchoFormulario = $('#formulario-login').outerWidth();
  var altoFormulario = $('#formulario-login').outerHeight();
  var posicionIzquierda = ($(window).width() - anchoFormulario) / 2;
  var posicionArriba = ($(window).height() - altoFormulario) / 2;
  $('#formulario-login').css({
  'position': 'fixed',
  'left': posicionIzquierda,
  'top': posicionArriba
  });
}


 